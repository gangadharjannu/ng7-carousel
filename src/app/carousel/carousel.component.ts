import {
  Component,
  AfterContentInit,
  OnDestroy,
  Input,
  ContentChildren,
  QueryList,
  ElementRef
} from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements AfterContentInit, OnDestroy {
  @Input() autoplay = true;
  @Input() interval = 3000;
  @Input() disableMobilePagination = true;

  @ContentChildren('carouselItem') carouselItems: QueryList<ElementRef>;

  itemIdx = 0;
  items: Array<HTMLDivElement> = [];
  paginationItems: Array<{ idx: number; active: boolean }>;
  timer: number;
  noCarousel = false;

  ngAfterContentInit() {
    if (this.carouselItems && this.carouselItems.length) {
      this.initGallery();
    } else {
      this.noCarousel = true;
    }
  }

  nextItem() {
    this.move(this.itemIdx + 1);
  }

  prevItem() {
    this.move(this.itemIdx - 1);
  }

  moveItem(idx: number) {
    if (this.disableMobilePagination && this.mobilecheck()) {
      return;
    }
    this.move(idx);
  }

  initGallery() {
    this.itemIdx = 0;
    this.items = this.carouselItems.map(item => item.nativeElement);
    this.paginationItems = this.items
      .map((x, idx) => ({
        idx,
        active: idx === this.itemIdx ? true : false
      }));

    this.items[this.itemIdx].style.opacity = '1';

    if (this.autoplay) {
      this.autoplayCarousel();
    }
  }

  move(nextIdx) {
    let carouselCurrentItem;
    let carouselNextItem;

    if (nextIdx > this.itemIdx) {
      if (nextIdx >= this.items.length) {
        nextIdx = 0;
      }
      carouselCurrentItem = 'moveLeftCurrent';
      carouselNextItem = 'moveLeftNext';
    } else if (nextIdx < this.itemIdx) {
      if (nextIdx < 0) {
        nextIdx = this.items.length - 1;
      }
      carouselCurrentItem = 'moveRightCurrent';
      carouselNextItem = 'moveRightPrev';
    }

    if (nextIdx !== this.itemIdx) {
      let currentItem;
      let nextItem;

      nextItem = this.items[nextIdx];
      currentItem = this.items[this.itemIdx];
      for (let i = 0; i < this.items.length; i++) {
        this.items[i].className = 'carousel-item';
        this.items[i].style.opacity = '0';
        this.paginationItems[i].active = false;
      }
      currentItem.classList.add(carouselCurrentItem);
      nextItem.classList.add(carouselNextItem);
      this.paginationItems[nextIdx].active = true;
      this.itemIdx = nextIdx;
    }
  }

  autoplayCarousel() {
    this.timer = window.setInterval(() => {
      this.move(this.itemIdx + 1);
    }, this.interval);
  }

  mobilecheck() {
    return (typeof window.orientation !== 'undefined')
      || (navigator.userAgent.indexOf('IEMobile') !== -1);
  }

  ngOnDestroy() {
    window.clearInterval(this.timer);
  }
}
