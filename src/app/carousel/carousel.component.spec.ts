import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { CarouselComponent } from './carousel.component';

@Component({
  selector: 'app-test',
  template: `<app-carousel autoplay="true" interval="5000" disable-mobile-pagination="true">
              <div #carouselItem class="carousel-item">
                <img src="../assets/carousel/home1.jpg" alt="home1" />
              </div>
              <div #carouselItem class="carousel-item">
                <img src="../assets/carousel/home2.jpg" alt="home2" />
              </div>
              <div #carouselItem class="carousel-item">
                <img src="../assets/carousel/home3.jpg" alt="home3" />
              </div>
              <div #carouselItem class="carousel-item">
                <img src="../assets/carousel/home4.jpg" alt="home4" />
              </div>
              <span>PROJECTED_CONTENT</span>
            </app-carousel>`
})
class TestComponent { }

describe('CarouselComponent', () => {
  let component: TestComponent;
  let carouselComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestComponent, CarouselComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    carouselComponent = fixture
      .debugElement
      .query(By.directive(CarouselComponent))
      .componentInstance;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should instantiate', () => {
    expect(component).toBeDefined();
  });

  describe('should project the content into carousel component', () => {
    it('should return PROJECTED_CONTENT', () => {
      expect(
        fixture.debugElement
          .query(By.css('app-carousel'))
          .nativeElement.innerHTML
      ).toContain('PROJECTED_CONTENT');
    });
    it('should contain 4 divs', () => {
      expect(
        fixture.debugElement
          .query(By.css('app-carousel'))
          .queryAll(By.css('div.carousel-item'))
          .length
      ).toEqual(4);
    });
  });

  describe('should have inputs ', () => {
    it('should call autoplayCarousel function', () => {
      carouselComponent.autoplay = true;
      carouselComponent.autoplayCarousel = jasmine.createSpy();
      carouselComponent.ngAfterContentInit();
      fixture.detectChanges();
      expect(carouselComponent.autoplayCarousel).toHaveBeenCalled();
    });
    it('should set interval as 5000', () => {
      fixture.detectChanges();
      expect(carouselComponent.interval).toEqual('5000');
    });
    it('should set interval as 5000', () => {
      fixture.detectChanges();
      expect(carouselComponent.interval).toEqual('5000');
    });
    it('should set disableMobilePagination as true', () => {
      fixture.detectChanges();
      expect(carouselComponent.disableMobilePagination).toEqual(true);
    });
    it('should not call move function if disableMobilePagination is true and the device is mobile', () => {
      carouselComponent.move = jasmine.createSpy();
      carouselComponent.ngAfterContentInit();
      fixture.detectChanges();
      expect(carouselComponent.move).not.toHaveBeenCalled();
    });
    it('should call move function if disableMobilePagination is true and the device is a mobile', () => {
      carouselComponent.move = jasmine.createSpy();
      spyOn(carouselComponent, 'mobilecheck').and.returnValues(true);
      carouselComponent.ngAfterContentInit();
      fixture.detectChanges();
      expect(carouselComponent.move).not.toHaveBeenCalled();
    });
  });
});
